@extends('app')

@section('main')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3">
        <h1 class="h2">Employees</h1>
    </div>

    @include('employees.partials.table',['data' => $employees])

@stop